import random 
words=['mango','orange','banana','apple','pineapple','jack','litchi','cherry','papaya','water']
def guessed_word():
    return words[random.randint(0,len(words)-1)].upper()
print('start the game')
print()
selected_word = guessed_word()
guesses = []
for i in range(len(selected_word)):
    guesses.append('_')
    print('_',end=' ')
print()
i = 0
chances = 6
while i < chances:
    guess=input('enter an alphabet  :  ').upper()
    if guess in selected_word:
        print('your guess is correct')
        a=0
        while guess != selected_word[a]:
            a=a+1
        else:
            guesses[a]=selected_word[a]
            guess_str=''
            guess_str=(guess_str.join(guesses))
            print(guess_str)
    else:
        print('your guess is wrong')
        print('you have',len(selected_word)-i,'more chances')
        i += 1
print()
word = ''
word = word.join(guesses)
if word == selected_word:
    print('you won')
else:
    print('you lose')
    print('play again')
    print('the correct word is: ',selected_word)


