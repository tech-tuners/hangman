import random
random_words = ['apple','orange','pineapple','jackfruit','kiwi','book','house','computer','photo','comb','rabbit','fruit','deer','lion','tiger','monkey','square','circle','school','stick','paper','blood','water','grain','hangman','shop']

def get_word():
    word = random.choice(random_words)
    return word.upper()
def game(word):
    display = "_"*len(word)
    guessed = False
    guessed_letters = []
    chances = 6

    print("welcome..lets start hangman game!")
    print(display)
    print("guess a",len(display),"letter word")
    print(" ")
    while not guessed and chances>0:
        guess = input("guess a letter : ").upper()
        if len(guess) == 1:
            if guess in guessed_letters:
                print("letter already guessed")
            elif guess not in word:
                print("oops, wrong guess!")
                chances -= 1
                guessed_letters.append(guess)
            else:
                print("hurray, correct guess!")
                guessed_letters.append(guess)
                word_list = list(display)
                indices = []
                for i,letter in enumerate(word):
                    if letter == guess:
                        indices.append(i)
                for i in indices:
                    word_list[i] = guess
                display = "".join(word_list)
                if "_" not in display:
                    guessed = True
        print(display)
        print(" ")


    if guessed:
        print("congrats you guessed, YOU WIN!")
    else:
        print("sorry YOU LOST!")


def main():
    word = get_word()
    game(word)

if __name__ == "__main__":
    main()